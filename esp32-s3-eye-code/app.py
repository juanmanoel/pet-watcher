import wifi
import socketpool
import board
import espcamera
from time import sleep
import ipaddress

print("PROJETO: PetMonitor UP")
print("ENDEREÇO IP: ", wifi.radio.ipv4_address)

# Inicialize o rádio WiFi
wifi.radio.connect("Verde_2G", "Hidrocorrox0")

# Crie um pool de sockets para o rádio WiFi
pool = socketpool.SocketPool(wifi.radio)

# Defina o endereço IP e a porta do servidor
server_address = "192.168.0.35"  # Substitua pelo endereço IP da sua placa IoT
server_port = 8001  # Substitua pela porta do servidor na sua placa IoT

# Crie um socket do tipo TCP a partir do pool
client_socket = pool.socket()

cam = espcamera.Camera(
    data_pins=board.CAMERA_DATA,
    external_clock_pin=board.CAMERA_XCLK,
    pixel_clock_pin=board.CAMERA_PCLK,
    vsync_pin=board.CAMERA_VSYNC,
    href_pin=board.CAMERA_HREF,
    pixel_format=espcamera.PixelFormat.RGB565,
    frame_size=espcamera.FrameSize.QVGA,
    i2c=board.I2C(),
    external_clock_frequency=20_000_000,
    framebuffer_count=1)


try:
        # Conecte-se ao servidor
        count = 0
        client_socket.connect((server_address, server_port))
        print("Conectado ao servidor:", server_address, "na porta:", server_port)
        ip1 = ipaddress.ip_address(server_address) 
        # Envie uma mensagem para o servidor
        while True:
          frame = cam.take(1)
          # criar detector de gato e carregar aqui 
          # se gato estiver na imagem enviar evento pelo comando abaixo
          # envia frame do gato detectado para jetson nano
          # envia frame para servidor jetson nano
          frame_bytes = bytes(frame)
          #client_socket.send(frame_bytes)
          message = f"{frame_bytes}"
          client_socket.send(message.encode())
          print("PROJETO: PetMonitor UP")
          print("ENDEREÇO IP: ", wifi.radio.ipv4_address)
          print("Servidor: ", server_address)
          print("Porta: ", server_port)
          print("ping:", wifi.radio.ping(ip1))
          sleep(1)

finally:
        # Encerre a conexão com o servidor
        client_socket.close()
        print("Conexão encerrada")

