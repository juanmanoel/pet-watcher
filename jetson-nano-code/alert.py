from discord_webhook import DiscordWebhook
from datetime import datetime

def enviar_alerta_discord(info):
    # URL do webhook do Discord
    url = 'https://discord.com/api/webhooks/SEU_WEBHOOK'

    # Criar um objeto de webhook
    webhook = DiscordWebhook(url=url)

    # Extrair informações do dicionário
    titulo = info.get('titulo', '')
    imagem = info.get('imagem', '')
    data_hora = info.get('datetime', datetime.now())

    # Definir o título da mensagem
    webhook.set_content(content=titulo)

    # Adicionar a imagem à mensagem, se fornecida
    if imagem:
        webhook.set_image(url=imagem)

    # Adicionar a data/hora à mensagem
    webhook.add_field(name='Data/Hora', value=data_hora.strftime('%d de %B de %Y, %H:%M'))

    # Enviar a mensagem para o canal do Discord
    webhook.execute()
