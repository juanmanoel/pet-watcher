import socket
from lodge import logger
import numpy as np
import cv2
import dataset
from datetime import datetime as dt
from alert import enviar_alerta_discord

# Defina o endereço IP e a porta do servidor
server_address = "0.0.0.0"  # Substitua pelo endereço IP da sua placa IoT
server_port = 8001  # Substitua pela porta do servidor na sua placa IoT

# Crie um socket do tipo TCP
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Faça o bind do socket ao endereço e porta desejados
server_socket.bind((server_address, server_port))

# Aguarde conexões
server_socket.listen(1)

print("Servidor aguardando conexões...")

db = dataset.connect('sqlite:///:memory:')

table = db['tabel_eventos']


while True:
    # Aceite a conexão do cliente
    client_socket, client_address = server_socket.accept()
    print("Cliente conectado:", client_address)

    try:
        while True:
            # Receba os dados enviados pelo cliente
            data = client_socket.recv(1024)
            if not data:
                # Nenhum dado recebido, encerre a conexão com o cliente
                break
            # Exiba os dados recebidos
            logger.info(data.decode())
            logger.info(type(data.decode()))
            info = dict(evento='Gato Detectado', datetime=str(dt.now()), imagem=data.decode()), 
            enviar_alerta_discord(info)
            # quando receber a imagem disparar para o discord como evento 
            # carregar evento para tabela de gato detectado 
            table.insert(dict(evento='Gato Detectado', datetime=str(dt.now())))


    except:
        # Encerre a conexão com o cliente
        client_socket.close()
        print("Conexão encerrada:", client_address)